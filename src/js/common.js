/**
 * In Frame
 *
 * @returns {boolean}
 * @url http://stackoverflow.com/a/326076
 */
function inIframe() {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}