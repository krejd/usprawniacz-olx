(function () {
    'use strict';

    /**
     * Don't allow this script to run when any of
     * OLX.pl pages is loaded in iframe.
     */
    if (inIframe()) {
        return;
    }

    Initializer.run();

    /**
     * Lock interval to check when URL changes.
     *
     * This is done in order to reapply all core code
     * once user moves from page to page or changes
     * search filters.
     */

    var currentURL = window.location.href;

    setInterval(function () {

        var checkUrl = window.location.href;

        if (currentURL !== checkUrl) {

            Initializer.run();
            currentURL = checkUrl;

        }

    }, 500);

})();