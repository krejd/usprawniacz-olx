var Initializer = {

    run: function () {

        // Parse all offers visible on the page

        Offers.parseOffersOnCurrentPage();

        // Delete unwanted offers from current page

        Deleter.deleteUnwantedInMemory();
        Saver.deleteBookmarkedInMemory();

        OfferDOMNormalizer.normalizePromotedOffers();

        // Includes all DOM elements necessary
        // for Offers handling

        OfferDOMInjector.run();

        // Check if preview mode is active

        Preview.applyOnPreviewModeActive();

        // Bind all events to newly created DOM elements

        Preview.bindEvents();

    }

};