var DBRemoved = {

    _removedIds: null,

    /**
     * This method has to run on start in order to get already
     * hidden IDs from LocalStorage. Otherwise, when called,
     * returns a reference of this._removedIds field.
     *
     * @returns array
     */
    getOffersIds: function () {

        if (this._removedIds === null) {

            var hiddenIdsLSNode = localStorage.getItem('removedIds');

            if (hiddenIdsLSNode === null) {

                this._removedIds = [];
                localStorage.setItem('removedIds', JSON.stringify(this._removedIds));

            } else {

                this._removedIds = JSON.parse(hiddenIdsLSNode);

            }

        }

        return this._removedIds;

    },

    addOfferId: function (id) {

        // Check if ID exists already

        if (this._removedIds.indexOf(id) === -1) {
            this._removedIds.push(id);
        }

    },

    /**
     * Updates Local Storage with contents
     * of _removedIds array.
     */
    save: function () {

        if (this._removedIds !== null) {

            localStorage.setItem('removedIds', JSON.stringify(this._removedIds));

        }

    }
};