var OfferDOMNormalizer = {

    /**
     * Normalize promoted offers
     *
     * Removes yellow background for all promoted
     * offers. Also, removes the badge from the
     * offers. Seriously, customers usually don't
     * need to see that garbage :)
     */
    normalizePromotedOffers: function () {

        // Remove yellow background

        $('.offer.bg-3').each(function (index, element) {
            $(element).addClass('bg-3-normalized');
        });

        // Remove badges

        $('.paid.abs').remove();

    }

};