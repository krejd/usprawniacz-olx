var Deleter = {

    /**
     * Delete Unwanted (Offers) In Memory
     */
    deleteUnwantedInMemory: function () {

        var removedOffersIds = DBRemoved.getOffersIds();
        var offers = Offers.getOffers();

        for (var offerId in offers) {

            if (removedOffersIds.includes(parseInt(offerId)) === true) {

                // Todo replace with "remove element"
                $(offers[offerId]).parent().remove();

            }

        }

    }
};