var Saver = {

    /**
     * Delete Bookmarked (Offers) In Memory
     */
    deleteBookmarkedInMemory: function () {

        var bookmarkedOffersIds = DBBookmarked.getOffersIds();
        var offers = Offers.getOffers();

        for (var offerId in offers) {

            if (bookmarkedOffersIds.includes(parseInt(offerId)) === true) {

                // Todo replace with "remove element"
                $(offers[offerId]).parent().remove();

            }

        }

    }
};