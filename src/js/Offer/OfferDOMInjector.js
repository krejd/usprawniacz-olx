var OfferDOMInjector = {

    run: function () {

        this.injectActionPanel();
        this.injectPreviewButton();
        this.injectBookmarkButton();
        this.injectDeleteButton();
        this.bindEvents();

    },

    injectActionPanel: function () {

        var offers = Offers.getOffers();

        // Inject action panel for each offer

        for (var offerId in offers) {

            var $tbody = $('tbody', offers[offerId]);

            $tbody.append('<tr><td class="offer-action-panel" colspan="3"></td></tr>');

        }

    },

    injectDeleteButton: function () {

        var offers = Offers.getOffers();

        for (var offerId in offers) {

            // Append Delete button

            var $actionPanel = $('.offer-action-panel', offers[offerId]);
            var $deleteLink = $('<a></a>');
            $deleteLink.attr('data-id', offerId);
            $deleteLink.addClass('offer-action-delete');
            $deleteLink.text('Usuń');

            $actionPanel.append($deleteLink);

        }

    },

    injectBookmarkButton: function () {

        var offers = Offers.getOffers();

        for (var offerId in offers) {

            // Append Delete button

            var $actionPanel = $('.offer-action-panel', offers[offerId]);
            var $bookmarkLink = $('<a></a>');
            $bookmarkLink.attr('data-id', offerId);
            $bookmarkLink.addClass('offer-action-bookmark');
            $bookmarkLink.text('Zapisz');

            $actionPanel.append($bookmarkLink);

        }

    },

    injectPreviewButton: function () {

        var offers = Offers.getOffers();

        for (var offerId in offers) {

            // Append Delete button

            var thumb = $('.thumb', offers[offerId]);

            thumb.addClass('offer-action-preview');
            thumb.attr('data-id', offerId);

            $(document).on('click', thumb, function (event) {
                event.preventDefault();
            });

        }

    },

    bindEvents: function () {

        // Delete

        $('.offers').on('click', '.offer-action-delete', function (event) {

            var offers = Offers.getOffers();
            var offerId = $(this).data('id');

            DBRemoved.addOfferId(offerId);
            DBRemoved.save();

            Preview.loadNextOfferFromPage();

            $(offers[offerId]).remove();

        });

        // Bookmark

        $('.offers').on('click', '.offer-action-bookmark', function (event) {

            var offers = Offers.getOffers();
            var offerId = $(this).data('id');

            DBBookmarked.addOfferId(offerId);
            DBBookmarked.save();

            Preview.loadNextOfferFromPage();

            $(offers[offerId]).remove();


        });

    }

};