var DBBookmarked = {

    _bookmarkedIds: null,

    /**
     * This method has to run on start in order to get already
     * saved IDs from LocalStorage. Otherwise, when called,
     * returns a reference of this._bookmarkedIds field.
     *
     * @returns array
     */
    getOffersIds: function () {

        if (this._bookmarkedIds === null) {

            var hiddenIdsLSNode = localStorage.getItem('bookmarkedIds');

            if (hiddenIdsLSNode === null) {

                this._bookmarkedIds = [];
                localStorage.setItem('bookmarkedIds', JSON.stringify(this._bookmarkedIds));

            } else {

                this._bookmarkedIds = JSON.parse(hiddenIdsLSNode);

            }

        }

        return this._bookmarkedIds;

    },

    addOfferId: function (id) {

        // Check if ID exists already

        if (this._bookmarkedIds.indexOf(id) === -1) {
            this._bookmarkedIds.push(id);
        }

    },

    /**
     * Updates Local Storage with contents
     * of _bookmarkedIds array.
     */
    save: function () {

        if (this._bookmarkedIds !== null) {

            localStorage.setItem('bookmarkedIds', JSON.stringify(this._bookmarkedIds));

        }

    }
};