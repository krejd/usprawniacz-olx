var Offers = {

    _visibleOffers: {},

    parseOffersOnCurrentPage: function () {

        var that = this;

        this._clearOffers();

        var offers = $('.offer');

        offers.each(function (index, offerEl) {

            var offerId = $('table', offerEl).data('id');
            that._visibleOffers[offerId] = offerEl;

        });

    },

    getOffers: function () {

        return this._visibleOffers;

    },

    /**
     * Offers need to be cleared everytime user changes
     * page or filters search. This is done in order to
     * keep memory as clean as possible and to make array
     * iterations smooth.
     *
     * @private
     */
    _clearOffers: function () {

        this._visibleOffers = {};

    }

};