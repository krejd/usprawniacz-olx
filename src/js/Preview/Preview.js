var Preview = {

    /**
     * ##########################
     * ############# FIELDS
     * ##########################
     */

    _previewModeEnabled: false,
    _currentlyDisplayingId: null,

    /**
     * ##########################
     * ############# METHODS
     * ##########################
     */

    /**
     * Is preview mode enabled
     *
     * @returns {boolean}
     */
    isPreviewModeEnabled: function () {

        return this._previewModeEnabled;

    },

    /**
     * Apply on preview mode active
     *
     * This must be called both from inside Initializer
     * and this.enterPreviewMode method. This is because
     * if user changes page, style needs to re-applied
     * to wrappers.
     */
    applyOnPreviewModeActive: function () {

        if (this.isPreviewModeEnabled() === true) {

            $('.wrapper').addClass('wrapper-left');

        }

    },

    /**
     * Enter preview mode
     *
     * Creates an iframe which can be used in order
     * to load offers from the page user is visiting.
     */
    enterPreviewMode: function () {

        var that = this;

        // Set flag

        this._previewModeEnabled = true;

        // This piece of code should unify all operations
        // done to DOM / CSS when user enters preview mode.
        // It's kept inside different method, because this
        // has to be called everytime user filters search
        // results or changes category.

        this.applyOnPreviewModeActive();

        // Build iframe
        // This also binds an "onload" event to the frame.
        // Everytime user changes link to preview, the code
        // inside onload() method is called.
        // This basically removes all clutter in the iframe.

        var iframe = document.createElement('iframe');
        iframe.setAttribute('id', 'previewcard');
        iframe.onload = function () {

            var iframeContents = $('iframe#previewcard').contents();

            iframeContents.find('header').remove();
            iframeContents.find('.breadcrumb').remove();
            iframeContents.find('footer').remove();
            iframeContents.find('.similarads').remove();
            iframeContents.find('.marginbott10.clr').remove();
            iframeContents.find('.fleft.fb_detailpage').remove();
            iframeContents.find('.next-link').remove();
            iframeContents.find('.prev-link').remove();
            iframeContents.find('.addedbymobile').remove();
            iframeContents.find('.otokredytbox').remove();

        };

        document.body.appendChild(iframe);

        // Add "close iframe" button

        var closeButton = document.createElement('a');
        closeButton.classList = 'previewcard-close';
        closeButton.innerHTML = '<span>⨯</span>';

        document.body.appendChild(closeButton);

        closeButton.addEventListener('click', function () {
            event.preventDefault();
            that.exitPreviewMode();
        });

    },

    /**
     * Change link
     *
     * This changes link inside iframe Preview Card.
     * Except from changing src attribute, it also calls
     * onload() event defined in enterPreviewMode() method.
     *
     * @param link
     * @param offerId
     */
    changeLink: function (link, offerId) {

        var iframe = document.getElementById('previewcard');
        iframe.src = link;

        this._currentlyDisplayingId = parseInt(offerId);

    },

    /**
     * Exit preview mode
     */
    exitPreviewMode: function () {

        // Set flag

        this._previewModeEnabled = false;

        // Remove unecessary elements

        $('iframe').remove();
        $('.previewcard-close').remove();
        $('.wrapper').removeClass('wrapper-left');

    },

    /**
     * Load link in preview window
     *
     * @param offerLink
     * @param offerId
     */
    loadLinkInPreviewWindow: function (offerLink, offerId) {

        // Check if preview mode is enabled

        if (this.isPreviewModeEnabled() === false) {

            this.enterPreviewMode();
            this.changeLink(offerLink, offerId);

        } else {

            this.changeLink(offerLink, offerId);

        }

    },

    /**
     * Load next offer from page
     *
     * This method is bound to "bookmark" and "delete"
     * buttons. It tries to find next visible offer on
     * the page and open it in iframe Preview Card.
     *
     * First, it tries to find next offer in the line
     * based on current offer is reviewing. If there
     * is no offer in the scope, then it tries to find
     * first from the very top of the page. If there
     * are no offers remaining on the page at all,
     * it just plays sound that tells user that
     * no more offers are available.
     *
     */
    loadNextOfferFromPage: function () {

        if (this.isPreviewModeEnabled() === true) {

            var findEl = $('table[data-id="' + this._currentlyDisplayingId + '"]');
            var parent = findEl.closest('tr');
            var sibling = $(parent).next();
            var link = $('a.thumb', sibling).attr('href');
            var id = $('a.thumb', sibling).attr('data-id');

            if (typeof link == 'undefined') {

                var sound = new Audio('http://www.freesound.org/data/previews/26/26777_128404-lq.mp3');
                sound.play();

            } else {

                this.changeLink(link, id);

            }

        }

    },

    /**
     * ##########################
     * ############# EVENTS
     * ##########################
     */

    bindEvents: function () {

        this._bindClickPreviewLink();
        this._bindScrollBehaviour();

    },

    /**
     * Bind Click Preview Link
     *
     * @private
     */
    _bindClickPreviewLink: function () {

        var that = this;

        $('.offers').on('click', '.offer-action-preview', function (event) {

            var offers = Offers.getOffers();
            var offerId = $(this).data('id');

            var $offerUrl = $('a.link', $(offers[offerId]));
            var offerLink = $offerUrl.attr('href');

            that.loadLinkInPreviewWindow(offerLink, offerId);

        });

    },

    /**
     * Bind Scroll Behaviour
     *
     * Prevents main page from scrolling when scrolling
     * inside iframe.
     *
     * @private
     */
    _bindScrollBehaviour: function () {

        $(document).on({
            mouseenter: function () {

                $('body').addClass('lock-scrolling');

            },
            mouseleave: function () {

                $('body').removeClass('lock-scrolling');

            }
        }, 'iframe#previewcard');

    }

};