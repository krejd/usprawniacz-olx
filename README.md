Usprawniacz OLX
===============
This is a complete source code for cross-browser, user-script extension that contains bunch of useful features to improve productivity when surfing OLX.pl pages.

How to compile?
===============

Nothing except `npm` is necessary to compile source code.

1. Install `npm` on your computer and run `npm install` command. That will include all necessary dependencies to `nodule_modules/` directory.
2. Use local `Grunt` to either compile files. Run `node_modules/grunt/bin/grunt default` command from project directory. This will create complete extension file in `dist/` directory.
3. That's it! You can now put the `dist/extension.js` file in your Tampermonkey manager.

Development
===========

`Grunt` dependencies include `grunt-contrib-connect` package which allows you to develop application without need to replace Tampermonkey each time you do the changes.

Here's step by step guide on how to follow that approach:

1. Add content of `test/tampermonkey.js` to your Tampermonkey script manager.
2. Run `node_modules/grunt/bin/grunt serve` command to bring up the server that will allow you to serve compiled `extension.js` file from local environment on `localhost:8002`.
3. Run another process with `node_modules/grunt/bin/grunt watch` which watches all changes done to SCSS / JS files across `src/` directory.

License
=======

MIT License

Copyright (c) 2016 Paweł "krejd" Węgrzyn

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.