// ==UserScript==
// @name         Usprawniacz OLX (dev-mode)
// @namespace    olx.pl/*
// @version      1.0
// @description  Umożliwia testowanie "Usprawniacza OLX"
// @author       Pawel "krejd" Wegrzyn
// @match        http://olx.pl/*
// @grant        none
// ==/UserScript==

// Generate random string chain
// Gist taken from
// http://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript

function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 20; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

// Bring back console!
// Gist taken from
// http://stackoverflow.com/a/7089553

function bringBackConsole() {
    var i = document.createElement('iframe');
    i.style.display = 'none';
    document.body.appendChild(i);
    window.console = i.contentWindow.console;
}

(function () {
    'use strict';

    var script = document.createElement("script");
    script.setAttribute("src", "http://localhost:8002/extension.js?" + makeid());
    document.body.appendChild(script);

    /**
     * Bring back the console!
     * Console was removed
     */

    bringBackConsole();

    /**
     * Check if script could be loaded from localhost server ran
     * by Grunt process. Listener fires only when file was found.
     */

    var scriptLoadedFlag = false;
    script.addEventListener('load', function () {
        scriptLoadedFlag = true;
    });

    /**
     * Timeout for check was set to 1000 ms. This gives enough
     * time to make sure script is on its place.
     */

    setTimeout(function () {

        if (!scriptLoadedFlag) {

            alert("Usprawniacz OLX\n\n" +
                "You are using this script in development mode. Unfortunately, it looks" +
                "like extension.js file could not be loaded. Please check if server" +
                "is running. If you keep having problems, please visit our GIT" +
                "repository for further reference.");

        }

    }, 1000);

})();