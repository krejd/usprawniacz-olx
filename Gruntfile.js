module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src: 'src/js/<%= pkg.name %>.js',
                dest: 'build/<%= pkg.name %>.min.js'
            }
        },

        concat: {
            dist: {
                src: [
                    'src/js/extension_header.js',

                    'src/js/Initializer.js',
                    'src/js/Offer/Offers.js',
                    'src/js/Offer/Deleter.js',
                    'src/js/Offer/DBRemoved.js',
                    'src/js/Offer/OfferDOMNormalizer.js',
                    'src/js/Offer/OfferDOMInjector.js',
                    'src/js/Offer/DBBookmarked.js',
                    'src/js/Offer/Saver.js',

                    'src/js/Preview/Preview.js',

                    'src/_compiled/style.js',
                    'src/js/common.js',
                    'src/js/extension_body.js'

                ],
                dest: 'dist/extension.js'
            }
        },

        watch: {
            scripts: {
                files: ['src/js/**/*.js', 'src/css/**/*.scss'],
                tasks: ['jshint', 'sass', 'css2js', 'concat'],
                options: {
                    interrupt: true
                }
            }
        },

        jshint: {
            all: ['src/js/*.js']
        },

        css2js: {
            style: {
                src: ['src/_compiled/style.css'],
                dest: 'src/_compiled/style.js'
            }
        },

        sass: {
            options: {
                sourceMap: false
            },
            dist: {
                files: {
                    'src/_compiled/style.css': 'src/css/style.scss'
                }
            }
        },

        connect: {
            server: {
                options: {
                    port: 8002,
                    hostname: '*',
                    base: './dist/',
                    keepalive: true
                }
            }
        }

    });

    // Load tasks

    require('load-grunt-tasks')(grunt);

    // Default task(s).
    grunt.registerTask('default', ['jshint', 'sass', 'css2js', 'concat', 'watch']);
    grunt.registerTask('serve', ['jshint', 'sass', 'css2js', 'concat', 'connect'])

};